// LeeLinLBM.cppLeeLinLBM.cpp


#include <iostream>
#include "math.h"
using namespace std;

#include "auxiliary.h"
#include "lattice.h"





int main(){
    printf("\n\n\n\n\t\t\thallo, ich bin LeeLinLBM.cpp\n\n");
    

// parameters:
    double rhoH = 1000.0;
    double rhoL = 1.0;
    double rhoSatL = rhoH;      // saturation densities
    double rhoSatV = rhoL; 
    double tauL = 0.1;  //0.1
    double tauV = 1.0;  //1.0
    double intWidth = 5.0;      // interface width
    double srfcTens = 0.0001;    
    double beta = 12.0*srfcTens/(p2(p2(rhoSatL - rhoSatV)) * intWidth);   // interaction amplitude
    double k = 1.5*intWidth*srfcTens/p2(rhoSatL - rhoSatV);             // related to magnitude of surface tension


//debugging parameters:
    bool debug = false;
    int observeX = 24;
    int observeY = 50;

// initializing lattice
	lattice lt(101,101,rhoH,rhoL,k, beta);
    //lt.setBar(20,20,intWidth);    
    int midX = 50;
    int midY = 50;
    int radius = 25;    
    lt.setBubble(midX,midY,radius,intWidth);
    
    printf("initial state:\n");

    lt.nd[observeX][observeY].dump();    
    
// iteration parametes:
    int itStep = 0;
    int itInterval = 1;            // the mean relative error of laplace's law is averaged over so many steps
    int itBreak = itInterval;
    int itStop =   4;
    bool converged = false;
    int convergenceCounter = 0;
    double* convergenceArray = new double[itInterval];


//-----------------iteration start--------------------
    printf("iteration starts with interface width = %f, surface tension = %f, beta = %.10f, k = %.10f\n\n",
        intWidth,srfcTens,beta,k);

    while (itStep < itStop && !converged ){
        printf("iteration step: %d\n",itStep+1);
        while (itStep < itBreak && itStep < itStop){

            // pre-streaming collision step with mixed/biased difference:
            lt.preStrCollAndStr(rhoH, rhoL, tauL, tauV);
            lt.macroscopic();
 			lt.postStrColl(rhoH, rhoL, tauL, tauV);			

            lt.pressureDiff(midX,midY,radius,intWidth,srfcTens,convergenceArray,convergenceCounter);
            convergenceCounter =+1;
            itStep += 1;
        }

        converged = lt.checkConvergence(convergenceArray, itInterval, 0.0001);
        convergenceCounter = 0;
        //lt.nd[observeX][observeY].dump();
        printf("progress:\t%.1f%%\trelative Error = %f\n\n",double(itStep)/itStop*100,lt.relError);
        itBreak += itInterval;
        if(debug){ getchar();}
    }
	
    if(converged){
        printf("iteration converged after %d steps with relative error %f\n",itStep,lt.relError);
    } else{
        printf("iteration not converged after %d steps\n",itStop);
    }
    

    
// save and plot data:
    //printf("after iteration:\n");
    //lt.nd[observeX][observeY].dump();

    // intensity map:
	FILE *pf;
    pf = fopen("densityMap.csv","w");
	for (int i = 0; i<lt.lenX; i++){
        for ( int j=0 ; j<lt.lenY ; j++){
            fprintf(pf,"%d \t %d \t %f \n",j,i, lt.nd[i][j].rho);
        }
        /*
        // for Matlab:
        for(int j = 0; j<lt.lenY-1; j++){
            fprintf(pf,"%f, ",lt.nd[i][j].rho);
        }
        fprintf(pf,"%f, ",lt.nd[i][lt.lenY-1].rho);
        fprintf(pf,"\n"); */
    }
    
    // for matlab:
    /*
    for (int j = 0; j<lenW-1; j++){
     dummy = gr[i][j].rho;
     fprintf(pf,"%f, ",dummy);
     }
     dummy = gr[i][lenW-1].rho;
     fprintf(pf,"%f ",dummy);
     fprintf(pf,"\n");		*/

    
	fclose(pf);
    printf("densityMap.csv created \n");

    // profile:
    FILE *pf2;
    pf2 = fopen("densityCut.csv","w");
    for ( int j=0 ; j<lt.lenX ; j++){
        fprintf(pf2,"%d \t %f \n",j, lt.nd[j][observeY].rho);
        //fprintf(pf2,"%d \t %f \n",j, lt.nd[j][observeY].prssr);
    }
    fclose(pf2);
    printf("densityCut.csv created \n");


	FILE *gp;
	gp = popen("gnuplot -persist","w");
    fprintf(gp, "set terminal x11 0\n");
    fprintf(gp, "plot \"densityMap.csv\" using 2:1:3 with image\n");

    //fprintf(gp, " set terminal x11 1\n");
    //fprintf(gp, "set yrange[-0.7:0.7]\n");
    fprintf(gp, "set grid\n");
    //fprintf(gp, "plot \"densityCut.csv\" title 'iterations :%d'\n",itStep);
    fclose(gp);










return 0;
}