O=-Wall -Wuninitialized

fluidSim : LeeLinLBM.o latticeNode.o lattice.o auxiliary.o
	llvm-g++ -o fluidSim LeeLinLBM.o latticeNode.o lattice.o auxiliary.o
lattice.o : lattice.cpp lattice.h 
	llvm-g++ $O -c lattice.cpp
latticeNode.o : latticeNode.cpp latticeNode.h
	llvm-g++ $O -c latticeNode.cpp
auxiliary.o : auxiliary.cpp auxiliary.h
	llvm-g++ $O -c auxiliary.cpp
clean:
	rm -f core fluidSim LeeLinLBM.o latticeNode.o lattice.o auxiliary.o