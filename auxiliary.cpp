// auxiliary.cpp

#include "auxiliary.h"
#include "math.h"
#include "stdlib.h"

double scpr(double* a, double* b){
	return a[0] * b[0] + a[1] * b[1];
}

int pb(int l, unsigned len){
    if(l<0){
		return len - abs(l)%len;
    }
	return l%len;
}

double p2(double alpha){
	return pow(alpha,2);
}

double p3(double alpha){
	return pow(alpha,3);
}

